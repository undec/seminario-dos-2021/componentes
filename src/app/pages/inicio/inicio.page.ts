import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {DataService} from '../../services/data.service';

interface Componente {
  icon: string;
  name: string;
  redirectTo: string;
}

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  componentes: Observable<Componente[]>;

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.componentes = this.dataService.getMenuOptions();
    console.log(this.componentes);
  }

}
